<!DOCTYPE html>
<html>
<head>
    <title>Create Contact</title>
    <link rel="stylesheet" type="text/css" href="public/css/styles.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="public/js/script.js"></script>
</head>
<body>
    <h1>Create Contact</h1>
    <form action="index.php?action=create_contact" method="post" onsubmit="return validateContactForm()">
        <label for="name">Name:</label>
        <input type="text" id="name" name="name" required>
        <label for="surname">Surname:</label>
        <input type="text" id="surname" name="surname" required>
        <label for="email">Email:</label>
        <input type="email" id="email" name="email" required>
        <input type="submit" value="Create Contact">
    </form>
    <h2>Link Clients</h2>
    <form id="linkClientForm">
        // hidden fieldto store contact ID
        <input type="hidden" id="contact_id" name="contact_id" value="<?php echo $contact->id; ?>">
        <label for="client_id">Client:</label>
        <select id="client_id" name="client_id">
            // loop through all clients to create options
            <?php foreach ($clients as $client): ?>
                <option value="<?php echo $client['id']; ?>"><?php echo $client['name']; ?></option>
            <?php endforeach; ?>
        </select>
        <button type="button" onclick="linkClient()">Link Client</button>
    </form>
    <script>
    function validateContactForm() {
        const name = document.getElementById('name').value;
        const surname = document.getElementById('surname').value;
        const email = document.getElementById('email').value;
        if (name === "" || surname === "" || email === "") {
            alert("All fields are required.");
            return false;
        }
        return true;
    }
    </script>

    <h2>Linked Clients</h2>
    <ul id="linkedClientsList">
        // loop through lnked clients to create list items
        <?php foreach ($linkedClients as $client): ?>
            <li>
                <?php echo $client['name']; ?>
                <button type="button" onclick="unlinkClient(<?php echo $contact->id; ?>, <?php echo $client['id']; ?>)">Unlink</button>
            </li>
        <?php endforeach; ?>
    </ul>
</body>
</html>
