<!DOCTYPE html>
<html>
<head>
    <title>Contacts</title>
    <link rel="stylesheet" type="text/css" href="public/css/styles.css">
</head>
<body>
    <h1>Contacts</h1>
    <a href="index.php?action=create_contact">Create Contact</a>
    <?php if (empty($contacts)): ?>
        <p>No contact(s) found.</p>
    <?php else: ?>
        <table>
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Surname</th>
                    <th>Email</th>
                    <th>NO. of linked clients</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($contacts as $contact): ?>
                    <tr>
                        <td style="text-align: left;"><?php echo htmlspecialchars($contact['name']); ?></td>
                        <td style="text-align: left;"><?php echo htmlspecialchars($contact['surname']); ?></td>
                        <td style="text-align: left;"><?php echo htmlspecialchars($contact['email']); ?></td>
                        <td><?php echo $contact['client_count']; ?></td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    <?php endif; ?>
</body>
</html>
