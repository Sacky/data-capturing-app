<!DOCTYPE html>
<html>
<head>
    <title>Clients</title>
    <link rel="stylesheet" type="text/css" href="public/css/styles.css">
</head>
<body>
    <h1>Clients</h1>
    <a href="index.php?action=create_client">Create Client</a>
    <?php if (empty($clients)): ?>
        <p>No client(s) found.</p>
    <?php else: ?>
        <table>
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Client Code</th>
                    <th>NO. of linked contacts</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($clients as $client): ?>
                    <tr>
                        <td style="text-align: left;"><?php echo htmlspecialchars($client['name']); ?></td>
                        <td style="text-align: left;"><?php echo htmlspecialchars($client['client_code']); ?></td>
                        <td><?php echo $client['contact_count']; ?></td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    <?php endif; ?>
</body>
</html>
