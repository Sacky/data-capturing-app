<!DOCTYPE html>
<html>
<head>
    <title>Create Client</title>
    <link rel="stylesheet" type="text/css" href="public/css/styles.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="public/js/script.js"></script>
</head>
<body>
    <h1>Create Client</h1>
    <form action="index.php?action=create_client" method="post" onsubmit="return validateClientForm()">
        <label for="name">Name:</label>
        <input type="text" id="name" name="name" required>
        <input type="submit" value="Create Client">
    </form>

    <h2>Link Contacts</h2>
    <form id="linkContactForm">
        // hidden field to store client ID
        <input type="hidden" id="client_id" name="client_id" value="<?php echo $client->id; ?>">
        <label for="contact_id">Contact:</label>
        <select id="contact_id" name="contact_id">
            // loop through all contacts to create options
            <?php foreach ($contacts as $contact): ?>
                <option value="<?php echo $contact['id']; ?>"><?php echo $contact['name'] . ' ' . $contact['surname']; ?></option>
            <?php endforeach; ?>
        </select>
        <button type="button" onclick="linkContact()">Link Contact</button>
    </form>

    <script>
    function validateClientForm() {
        const name = document.getElementById('name').value;
        if (name === "") {
            alert("Name is required.");
            return false;
        }
        return true;
    }
    </script>
    
    <h2>Linked Contacts</h2>
    <ul id="linkedContactsList">
        <?php foreach ($linkedContacts as $contact): ?>
            <li>
                <?php echo $contact['name'] . ' ' . $contact['surname']; ?>
                <button type="button" onclick="unlinkContact(<?php echo $client->id; ?>, <?php echo $contact['id']; ?>)">Unlink</button>
            </li>
        <?php endforeach; ?>
    </ul>
</body>
</html>
