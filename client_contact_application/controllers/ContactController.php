<?php
require_once 'lib/Controller.php'; // include the controller model
require_once 'models/Contact.php'; // include the contact model

class ContactController extends Controller {
    // display the list of all contacts
    public function index() {
        $contact = new Contact();
        $contacts = $contact->getContacts();
        // load the view to display the contacts
        require 'views/contacts/index.php';
    }
    // handle contact creation
    public function create() {
        try {
            $contact = new Contact();
        if ($_POST) {
            $name = $_POST['name'];
            $surname = $_POST['surname'];
            $email = $_POST['email'];
            $contact->createContact($name, $surname, $email);
            header("Location: index.php?action=contacts");
        }
        require 'views/contacts/form.php';
        // catch exceptions and return user friendly error messages
        } catch (Exception $e) {
            echo "Error: " . $e->getMessage();
        }
        
    }
    // link a client to acontact
    public function linkClient() {
        if ($_POST) {
            $contactId = $_POST['contact_id'];
            $clientId = $_POST['client_id'];
            $contact = new Contact();
            $contact->linkClient($contactId, $clientId);
            echo json_encode(['status' => 'success']);
        } else {
            echo json_encode(['status' => 'error']);
        }
    }
    // unlink a client from a contract
    public function unlinkClient() {
        if ($_POST) {
            $contactId = $_POST['contact_id'];
            $clientId = $_POST['client_id'];
            $contact = new Contact();
            $contact->unlinkClient($contactId, $clientId);
            echo json_encode(['status' => 'success']);
        } else {
            echo json_encode(['status' => 'error']);
        }
    }
    
}
?>
