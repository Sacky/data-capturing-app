<?php
require_once 'lib/Controller.php';
require_once 'models/Client.php';

class ClientController extends Controller {
    public function index() {
        $client = new Client();
        $clients = $client->getClients();
        require 'views/clients/index.php';
    }

    public function create() {
        try {
            $client = new Client();
        if ($_POST) {
            $name = $_POST['name'];
            $client->createClient($name);
            header("Location: index.php?action=clients");
        }
        require 'views/clients/form.php';
        // catch exceptions and return user friendly error messages
        } catch (Exception $e) {
            echo "Error: " . $e->getMessage();
        }
        
    }
    // link a contact to a client
    public function linkContact() {
        if ($_POST) {
            $clientId = $_POST['client_id'];
            $contactId = $_POST['contact_id'];
            $client = new Client();
            $client->linkContact($clientId, $contactId);
            echo json_encode(['status' => 'success']);
        } else {
            echo json_encode(['status' => 'error']);
        }
    }
    // unlink a contact from a client
    public function unlinkContact() {
        if ($_POST) {
            $clientId = $_POST['client_id'];
            $contactId = $_POST['contact_id'];
            $client = new Client();
            $client->unlinkContact($clientId, $contactId);
            echo json_encode(['status' => 'success']);
        } else {
            echo json_encode(['status' => 'error']);
        }
    }
    
}
?>
