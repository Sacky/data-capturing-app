<?php
require_once 'lib/Database.php';

class Client {
    private $db;

    public function __construct() {
        $this->db = new Database();
    }

    public function getClients() {
        $sql = "SELECT clients.id, clients.name, clients.client_code, COUNT(client_contact.contact_id) as contact_count
                FROM clients
                LEFT JOIN client_contact ON clients.id = client_contact.client_id
                GROUP BY clients.id
                ORDER BY clients.name ASC";
        return $this->db->fetchAll($sql);
    }

    public function createClient($name) {
        $clientCode = $this->generateClientCode($name);
        $sql = "INSERT INTO clients (name, client_code) VALUES (:name, :client_code)";
        $this->db->execute($sql, ['name' => $name, 'client_code' => $clientCode]);
    }

    private function generateClientCode($name) {
        $prefix = strtoupper(substr($name, 0, 3));
        while (strlen($prefix) < 3) {
            $prefix .= 'A';
        }
        $sql = "SELECT MAX(CAST(SUBSTRING(client_code, 4) AS UNSIGNED)) as max_code FROM clients WHERE client_code LIKE :prefix";
        $result = $this->db->fetch($sql, ['prefix' => $prefix . '%']);
        $number = $result['max_code'] + 1;
        return $prefix . str_pad($number, 3, '0', STR_PAD_LEFT);
    }
    // link a contact to a client
    public function linkContact($clientId, $contactId) {
        $sql = "INSERT INTO client_contact (client_id, contact_id) VALUES (:client_id, :contact_id)";
        $this->db->execute($sql, ['client_id' => $clientId, 'contact_id' => $contactId]);
    }
    // unlink a contact from a client
    public function unlinkContact($clientId, $contactId) {
        $sql = "DELETE FROM client_contact WHERE client_id = :client_id AND contact_id = :contact_id";
        $this->db->execute($sql, ['client_id' => $clientId, 'contact_id' => $contactId]);
    }
    
}
?>
