<?php
require_once 'lib/Database.php';

class Contact {
    private $db;

    public function __construct() {
        $this->db = new Database();
    }

    public function getContacts() {
        $sql = "SELECT contacts.id, contacts.name, contacts.surname, contacts.email, COUNT(client_contact.client_id) as client_count
                FROM contacts
                LEFT JOIN client_contact ON contacts.id = client_contact.contact_id
                GROUP BY contacts.id
                ORDER BY contacts.surname ASC, contacts.name ASC";
        return $this->db->fetchAll($sql);
    }

    public function createContact($name, $surname, $email) {
        $sql = "INSERT INTO contacts (name, surname, email) VALUES (:name, :surname, :email)";
        $this->db->execute($sql, ['name' => $name, 'surname' => $surname, 'email' => $email]);
    }
    // link a client to a contact
    public function linkClient($contactId, $clientId) {
        $sql = "INSERT INTO client_contact (client_id, contact_id) VALUES (:client_id, :contact_id)";
        $this->db->execute($sql, ['client_id' => $clientId, 'contact_id' => $contactId]);
    }
    // unlink a client from a contact
    public function unlinkClient($contactId, $clientId) {
        $sql = "DELETE FROM client_contact WHERE contact_id = :contact_id AND client_id = :client_id";
        $this->db->execute($sql, ['contact_id' => $contactId, 'client_id' => $clientId]);
    }
    
}
?>
