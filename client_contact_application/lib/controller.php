<?php
class Controller {
    public function loadModel($model) {
        require_once 'models/' . $model . '.php';
        return new $model();
    }

    public function render($view, $data = []) {
        extract($data);
        require 'views/' . $view . '.php';
    }
}
?>
