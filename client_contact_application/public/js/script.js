$(document).ready(function() {
    // client-side validation for forms
    $("form").submit(function(event) {
        let valid = true;
        $("input[type='text'], input[type='email']").each(function() {
            if ($(this).val() === "") {
                valid = false;
                alert("Please fill all required fields.");
                return false;
            }
        });
        if (!valid) {
            event.preventDefault();
        }
    });
});
// link a contact to a client using AJAX
function linkContact() {
    $.post('index.php?action=link_contact', $('#linkContactForm').serialize(), function(response) {
        if (response.status === 'success') {
            location.reload();
        } else {
            alert('Failed to link contact.');
        }
    }, 'json');
}
// unlink a contact from a client using AJAX
function unlinkContact(clientId, contactId) {
    $.post('index.php?action=unlink_contact', { client_id: clientId, contact_id: contactId }, function(response) {
        if (response.status === 'success') {
            location.reload();
        } else {
            alert('Failed to unlink contact.');
        }
    }, 'json');
}
// link a client to a contact using AJAX
function linkClient() {
    $.post('index.php?action=link_client', $('#linkClientForm').serialize(), function(response) {
        if (response.status === 'success') {
            location.reload();
        } else {
            alert('Failed to link client.');
        }
    }, 'json');
}
// unlink a client from a contact using AJAX
function unlinkClient(contactId, clientId) {
    $.post('index.php?action=unlink_client', { contact_id: contactId, client_id: clientId }, function(response) {
        if (response.status === 'success') {
            location.reload();
        } else {
            alert('Failed to unlink client.');
        }
    }, 'json');
}
