<?php
require '../controllers/ClientController.php';
require '../controllers/ContactController.php';

$action = isset($_GET['action']) ? $_GET['action'] : '';

switch ($action) {
    case 'clients':
        $controller = new ClientController();
        $controller->index();
        break;
    case 'create_client':
        $controller = new ClientController();
        $controller->create();
        break;
    case 'contacts':
        $controller = new ContactController();
        $controller->index();
        break;
    case 'create_contact':
        $controller = new ContactController();
        $controller->create();
        break;
    default:
        echo "Unknown action.";
        break;
}
?>
